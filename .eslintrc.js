/* global module */

module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: 'eslint:recommended',
    overrides: [
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    rules: {
        'array-bracket-spacing': ['error', 'never'],
        'arrow-spacing': 'error',
        'block-scoped-var': 'error',
        'brace-style': ['error', 'stroustrup'],
        'comma-dangle': ['error', 'always-multiline'],
        'func-call-spacing': ['error', 'never'],
        'keyword-spacing': 'error',
        'linebreak-style': ['error', 'unix'],
        'lines-between-class-members': ['error', 'always'],
        'no-else-return': 'error',
        'no-trailing-spaces': 'error',
        'object-curly-spacing': ['error', 'never'],
        'padding-line-between-statements': [
            'error',
            {
                blankLine: 'always',
                prev: ['const', 'let', 'var'],
                next: '*',
            },
            {
                blankLine: 'any',
                prev: ['const', 'let', 'var'],
                next: ['const', 'let', 'var'],
            },
        ],
        'prefer-const': 'error',
        'quote-props': ['error', 'as-needed'],
        'space-before-function-paren': ['error', 'never'],
        camelcase: 'error',
        curly: ['error', 'all'],
        eqeqeq: 'error',
        indent: ['error', 4],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
    },
};
