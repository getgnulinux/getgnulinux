SUBDIRS = data po docroot

c_green = \e[32m
c_reset = \e[0m

NODEJS = nodejs
RSYNC = rsync
RSYNC_OPTS = --recursive --checksum --exclude-from .rsync-exclude --delete --delete-excluded --progress
DEPLOY_HOST = tuxfamily
DEPLOY_PATH = /home/getgnulinux/getgnulinux.org-web/htdocs


.PHONY: all
all: lint
	$(MAKE) -C po update-gmo install
	npm run build

.PHONY: lint
lint: eslint stylelint

.PHONY: eslint
eslint:
	@echo "Checking JavaScript code..."
	@npx eslint $(ESLINT_OPTS) *.js src/*.js
	@echo -e "$(c_green)OK$(c_reset)"

.PHONY: eslint-fix
eslint-fix: ESLINT_OPTS = --fix
eslint-fix: eslint

.PHONY: stylelint
stylelint:
	@echo "Checking SCSS code..."
	@npx stylelint $(STYLELINT_OPTS) "src/styles/*.scss"
	@echo -e "$(c_green)OK$(c_reset)"

.PHONY: stylelint-fix
stylelint-fix: STYLELINT_OPTS = --fix
stylelint-fix: stylelint

.PHONY: update-po
update-po: po
	$(MAKE) -C $< $@

.PHONY: deploy-dev
deploy-dev: /var/www/html
	@$(RSYNC) $(RSYNC_OPTS) docroot/ /var/www/html

.PHONY: deploy
deploy:
	@$(RSYNC) $(RSYNC_OPTS) --compress docroot/ $(DEPLOY_HOST):$(DEPLOY_PATH)
