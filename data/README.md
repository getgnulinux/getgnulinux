# Updating translation credits

Translation credits can be updated as follows.

1. In Weblate, go to **Insights** > **Translation reports**.
2. Set **Report format** to **JSON**.
3. Set **Report period** to Jan 2011 until today.
4. Set **Language** to **All languages**.
5. Click **Generate** and save the JSON report as
   `data/hosted.weblate.org.json` in the project directory.
6. From the Docker container, run `make -C data weblate_credits.json` to update
   the credits file.
7. From the Docker container, run `make` to update the credits page.
8. Commit your changes.
