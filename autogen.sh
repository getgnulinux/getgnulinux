#!/bin/sh

set -e

# Install gettext infrastructure
test -e po/Makefile.in.in || gettextize

# Update generated configuration files
autoreconf -vi
